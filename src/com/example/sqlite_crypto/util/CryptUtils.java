package com.example.sqlite_crypto.util;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.util.Log;
import net.sqlcipher.database.SQLiteDatabase;

public class CryptUtils {

	public static void ConvertNormalToSQLCipheredDB(Context context,
			String startingFileName, String endingFileName, String filePassword)
			throws IOException {
		File mStartingFile = context.getDatabasePath(startingFileName);
		if (!mStartingFile.exists()) {
			return;
		}
		File mEndingFile = context.getDatabasePath(endingFileName);
		mEndingFile.delete();
		SQLiteDatabase database = null;
		try {
			database = SQLiteDatabase.openOrCreateDatabase(mStartingFile, "", null);
			database.rawExecSQL(String.format(
					"ATTACH DATABASE '%s' AS encrypted KEY '%s'",
					mEndingFile.getAbsolutePath(), filePassword));
			database.rawExecSQL("select sqlcipher_export('encrypted')");
			database.rawExecSQL("DETACH DATABASE encrypted");
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (database.isOpen())
				database.close();
			//mStartingFile.delete();
		}
	}

	public static void decryptDatabase(File encrypt, File decrypt, String pass) {
		File unencryptedFile = decrypt;
		
		unencryptedFile.delete();
		try{
			unencryptedFile.createNewFile();
		}catch(IOException e){
			Log.i("Cant create new ",e.toString());
		}
		
		File databaseFile = encrypt;

		SQLiteDatabase database = SQLiteDatabase.openOrCreateDatabase(
				databaseFile, pass, null, null); // Exception
		if (database.isOpen()) {
			database.rawExecSQL(String.format(
					"ATTACH DATABASE '%s' as plaintext KEY '';",
					unencryptedFile.getAbsolutePath()));
			database.rawExecSQL("SELECT sqlcipher_export('plaintext');");
			database.rawExecSQL("DETACH DATABASE plaintext;");
			android.database.sqlite.SQLiteDatabase sqlDB = android.database.sqlite.SQLiteDatabase
					.openOrCreateDatabase(unencryptedFile, null);
			sqlDB.close();
			database.close();
		}

		// databaseFile.delete();
	}

}
